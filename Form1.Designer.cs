﻿namespace AudioPlayer
{
    partial class Phantom
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Phantom));
            this.PlayList = new System.Windows.Forms.ListBox();
            this.file = new System.Windows.Forms.Button();
            this.play = new System.Windows.Forms.Button();
            this.next = new System.Windows.Forms.Button();
            this.stop = new System.Windows.Forms.Button();
            this.pause = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tracktime = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.Volume = new System.Windows.Forms.TrackBar();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tracktime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Volume)).BeginInit();
            this.SuspendLayout();
            // 
            // PlayList
            // 
            this.PlayList.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.PlayList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PlayList.FormattingEnabled = true;
            resources.ApplyResources(this.PlayList, "PlayList");
            this.PlayList.Name = "PlayList";
            this.PlayList.SelectedIndexChanged += new System.EventHandler(this.PlayList_SelectedIndexChanged);
            this.PlayList.DoubleClick += new System.EventHandler(this.play_Click);
            // 
            // file
            // 
            this.file.BackgroundImage = global::AudioPlayer.Properties.Resources.playlist_2717;
            resources.ApplyResources(this.file, "file");
            this.file.Name = "file";
            this.file.UseVisualStyleBackColor = true;
            this.file.Click += new System.EventHandler(this.file_Click);
            // 
            // play
            // 
            this.play.BackgroundImage = global::AudioPlayer.Properties.Resources.player_play_5144;
            resources.ApplyResources(this.play, "play");
            this.play.Name = "play";
            this.play.UseVisualStyleBackColor = true;
            this.play.Click += new System.EventHandler(this.play_Click);
            // 
            // next
            // 
            this.next.BackgroundImage = global::AudioPlayer.Properties.Resources.player_fwd_9078;
            resources.ApplyResources(this.next, "next");
            this.next.Name = "next";
            this.next.UseVisualStyleBackColor = true;
            this.next.Click += new System.EventHandler(this.button4_Click);
            // 
            // stop
            // 
            this.stop.BackgroundImage = global::AudioPlayer.Properties.Resources.player_stop_2252;
            resources.ApplyResources(this.stop, "stop");
            this.stop.Name = "stop";
            this.stop.UseVisualStyleBackColor = true;
            this.stop.Click += new System.EventHandler(this.button3_Click);
            // 
            // pause
            // 
            this.pause.BackgroundImage = global::AudioPlayer.Properties.Resources.player_pause_9216;
            resources.ApplyResources(this.pause, "pause");
            this.pause.Name = "pause";
            this.pause.UseVisualStyleBackColor = true;
            this.pause.Click += new System.EventHandler(this.pause_Click);
            // 
            // back
            // 
            this.back.BackgroundImage = global::AudioPlayer.Properties.Resources.player_start_5763;
            resources.ApplyResources(this.back, "back");
            this.back.Name = "back";
            this.back.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Name = "label1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // tracktime
            // 
            this.tracktime.BackColor = System.Drawing.SystemColors.AppWorkspace;
            resources.ApplyResources(this.tracktime, "tracktime");
            this.tracktime.Maximum = 99;
            this.tracktime.Name = "tracktime";
            this.tracktime.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // Volume
            // 
            this.Volume.BackColor = System.Drawing.SystemColors.AppWorkspace;
            resources.ApplyResources(this.Volume, "Volume");
            this.Volume.Maximum = 100;
            this.Volume.Name = "Volume";
            this.Volume.Value = 100;
            this.Volume.Scroll += new System.EventHandler(this.Volume_Scroll);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.Form1_Load);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // Phantom
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Volume);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tracktime);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.file);
            this.Controls.Add(this.play);
            this.Controls.Add(this.next);
            this.Controls.Add(this.stop);
            this.Controls.Add(this.pause);
            this.Controls.Add(this.back);
            this.Controls.Add(this.PlayList);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.HelpButton = true;
            this.Name = "Phantom";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tracktime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Volume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox PlayList;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Button pause;
        private System.Windows.Forms.Button stop;
        private System.Windows.Forms.Button next;
        private System.Windows.Forms.Button play;
        private System.Windows.Forms.Button file;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar tracktime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar Volume;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label3;
    }
}

