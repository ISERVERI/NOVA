![](https://github.com/slipNET/NOVA/blob/master/screenshots/Nova.jpg)


__Введение__

Nova - это аудиопроигрыватель с открытым исходным кодом для персонального компьютера, 
поддерживает форматы aac, ac3, ape, flac, wma, midi, opus. 
***
__Introduction__

Nova is an open source audio player for a personal computer, 
supports aac, ac3, ape, flac, wma, midi, opus formats.
***
__Поддержать проект__

Рекомендации по проекту высылать на электронную почту: slipcast@mail.ru

Желающие помочь или поддержать проект пишите на электронную почту: slipcast@mail.ru
***
Support the project

Send project recommendations by e-mail: slipcast@mail.ru

Those wishing to help or support the project write to e-mail: slipcast@mail.ru
