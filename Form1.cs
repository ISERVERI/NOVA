﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AudioPlayer
{
    public partial class Phantom : Form
    {
        public Phantom()
        {
            InitializeComponent();
            BassLike.InitBass(BassLike.Hz);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Text = TimeSpan.FromSeconds(BassLike.GetPosOfStream(BassLike.Stream)).ToString();
           // tracktime.Value = BassLike.GetPosOfStream(BassLike.Stream);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BassLike.stop();
            timer1.Enabled = false;
            Volume.Value = 0;
            label1.Text = "00:00:00";
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void play_Click(object sender, EventArgs e)
        {
            if((PlayList.Items.Count != 0) && (PlayList.SelectedIndex != -1))
            {
                string current = Main.Files[PlayList.SelectedIndex];
                BassLike.play(current, BassLike.Volume);
                label1.Text = TimeSpan.FromSeconds(BassLike.GetPosOfStream(BassLike.Stream)).ToString();
                label2.Text = TimeSpan.FromSeconds(BassLike.GetTimeOfStream(BassLike.Stream)).ToString();
                tracktime.Maximum = BassLike.GetTimeOfStream(BassLike.Stream);
                Volume.Value = BassLike.GetPosOfStream(BassLike.Stream);
                timer1.Enabled = true;
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            BassLike.SetPosOfScroll(BassLike.Stream, tracktime.Value);
        }

        private void file_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            Main.Files.Add(openFileDialog1.FileName);
            PlayList.Items.Add(Main.GetFileName(openFileDialog1.FileName));
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
      
        private void Volume_Scroll(object sender, EventArgs e)
        {
            BassLike.SetVolumeToStream(BassLike.Stream, Volume.Value);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void pause_Click(object sender, EventArgs e)
        {
            BassLike.pause();
        }

        private void PlayList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
