﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioPlayer
{

    public static class Main
    {
        /// <summary>
        /// путь к исполняемому фаилу
        /// </summary>
        public static string AppPath = AppDomain.CurrentDomain.BaseDirectory;

        /// <summary>
        /// список полных имен фаилов
        /// </summary>
        public static List<string>  Files = new List<string>();

        public static string GetFileName(string file)
        {
            string[] tmp = file.Split('/');
            return tmp[tmp.Length - 1];
        }

    }
}
