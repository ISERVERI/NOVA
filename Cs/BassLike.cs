﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Un4seen.Bass;
using System.Windows.Forms;

namespace AudioPlayer

{
    public static class BassLike
    {
        /// <summary>
        /// частота дискретизации
        /// </summary>
        public static int Hz = 44100;
        /// <summary>
        /// состояние инициализации
        /// </summary>
        public static bool InitDefaultDevice;
        /// <summary>
        /// Канал потока в библиотеке Bass dll
        /// </summary>
        public static int Stream;
        /// <summary>
        /// громкость
        /// </summary>
        public static int Volume = 100;

        private static readonly List<int> BassPluginsHandles = new List<int>();

        /// <summary>
        /// Иницилизация библиотеки Bass dll
        /// </summary>
        /// <param name="Hz"></param>
        /// <returns></returns>
        public static bool InitBass(int Hz)
        {
            if (!InitDefaultDevice)
            {
                InitDefaultDevice = Bass.BASS_Init(-1, Hz, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);
                if (InitDefaultDevice)
                {
                    BassPluginsHandles.Add(Bass.BASS_PluginLoad(Main.AppPath + @"plugins\bass_aac.dll"));
                    BassPluginsHandles.Add(Bass.BASS_PluginLoad(Main.AppPath + @"plugins\bass_ac3.dll"));
                    BassPluginsHandles.Add(Bass.BASS_PluginLoad(Main.AppPath + @"plugins\bass_ape.dll"));
                    BassPluginsHandles.Add(Bass.BASS_PluginLoad(Main.AppPath + @"plugins\bass_flac.dll"));
                    BassPluginsHandles.Add(Bass.BASS_PluginLoad(Main.AppPath + @"plugins\bass_wma.dll"));
                    BassPluginsHandles.Add(Bass.BASS_PluginLoad(Main.AppPath + @"plugins\bassmidi.dll"));
                    BassPluginsHandles.Add(Bass.BASS_PluginLoad(Main.AppPath + @"plugins\bassopus.dll"));

                    int ErrorCount = 0;
                    for (int i = 0; i < BassPluginsHandles.Count; i++)
                        if (BassPluginsHandles[i] == 0)
                            ErrorCount++;
                    if (ErrorCount!= 0)
                        MessageBox.Show(ErrorCount + " плагинов не было загружено", " ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }


            }



            return InitDefaultDevice;
        }

        /// <summary>
        /// воспроизведение
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="vol"></param>
        public static void play(string filename, int vol)
        {
            if (Bass.BASS_ChannelIsActive(Stream) != BASSActive.BASS_ACTIVE_PAUSED)
            {
                stop();
                if (InitBass(Hz))
                {
                    Stream = Bass.BASS_StreamCreateFile(filename, 0, 0, BASSFlag.BASS_DEFAULT);
                    if (Stream != 0)
                    {
                        Volume = vol;
                        Bass.BASS_ChannelSetAttribute(Stream, BASSAttribute.BASS_ATTRIB_VOL, Volume / 100);
                        Bass.BASS_ChannelPlay(Stream, false);
                    }
                }
            }
            else
                Bass.BASS_ChannelPlay(Stream, false);
        }
        /// <summary>
        /// Стоп
        /// </summary>
        public static void stop()
        {
            Bass.BASS_ChannelStop(Stream);
            Bass.BASS_StreamFree(Stream);
        }

        ///пауза
       public static void pause()
        {
            if (Bass.BASS_ChannelIsActive(Stream) == BASSActive.BASS_ACTIVE_PLAYING)
                Bass.BASS_ChannelPause(Stream);
        }

        /// <summary>
        /// получение длительности канала в секундах
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static int GetTimeOfStream(int stream)
        {
            long TimeBytes = Bass.BASS_ChannelGetLength(stream);
            double Time = Bass.BASS_ChannelBytes2Seconds(stream, TimeBytes);
            return (int)Time;
        }

        /// <summary>
        /// получение позиции в секундах
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static int GetPosOfStream(int stream)
        {
            long pos = Bass.BASS_ChannelGetPosition(stream);
            int posSec = (int)Bass.BASS_ChannelBytes2Seconds(stream, pos);
            return posSec;
        }

        public static void SetPosOfScroll(int stream, int pos)
        {
            Bass.BASS_ChannelSetPosition(stream, (double)pos);
        }

        /// <summary>
        /// установка громкости
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="vol"></param>
        public static void SetVolumeToStream(int stream, int vol)
        {
            Volume = vol;
            Bass.BASS_ChannelSetAttribute(stream, BASSAttribute.BASS_ATTRIB_VOL, Volume / 100F);
        }


    }
}
